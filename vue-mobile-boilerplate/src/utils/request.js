import { createApi, encrypt } from '@halobear/vue-request'
import qs from 'kuan-utils/lib/qs'
import authority from './authority'

const key = 'f80ac7a2ff760bbee1fe2ec347c34805'

const request = createApi({
	setHeaders: config => {
		const { params = {}, data = {} } = config
		const now = Date.parse(new Date()) / 1000
		const { sourse_id = '3643Y8' } = qs.parse() || {}
		const headers = {
			'X-Halo-App': 'act-api',
			'X-Halo-Source': sourse_id,
			'Http-Request-Halo-Time': now,
			'Http-Request-Halo-Sign': encrypt({ ...params, ...data, time: now }, key),
		}
		const { token = '' } = authority.get() || {}
		if (token) {
			headers.Authorization = `Bearer ${token}`
		}
		return headers
	},
	loginForce: () => {},
})

export default request
