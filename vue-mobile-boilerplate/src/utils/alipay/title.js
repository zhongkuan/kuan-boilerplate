import { ready, isAliPay } from './utils'

// 设置标题
export async function setTitle(title) {
  if (!title) return
  if (!isAliPay) {
    document.title = title
  } else {
    await ready()
    window.AlipayJSBridge.call('setTitle', {
      title
    })
  }
}
