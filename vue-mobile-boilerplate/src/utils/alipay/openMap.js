import { loadSdk, ready } from './utils'

export default async function openMap({
  longitude = 116.39,
  latitude = 39.9,
  name = '',
  address = ''
}) {
  await loadSdk()
  await ready()
  ap.openLocation({
    longitude,
    latitude,
    name,
    address
  })
}
