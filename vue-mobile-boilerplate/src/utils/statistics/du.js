/* eslint-disable no-undef */
let isLoad = false

// 初始化统计
export function init(id) {
	if (!id) return
	var hm = document.createElement('script')
	hm.src = `https://hm.baidu.com/hm.js?${id}`
	var s = document.getElementsByTagName('script')[0]
	s.parentNode.insertBefore(hm, s)
	isLoad = true
	return new Promise(resolve => {
		s.onload = resolve
	})
}

// 统计页面访问
export async function trackPageView(href) {
	if (!isLoad) return
	if (typeof _hmt === 'undefined') return
	_hmt.push(['_trackPageview', href])
}

// 事件统计
export function trackEvent(category, action, opt_label, opt_value) {
	if (!isLoad) return
	if (typeof _hmt === 'undefined') return
	_hmt.push(['_trackEvent', category, action, opt_label, opt_value])
}

export default {
	init,
	trackPageView,
	trackEvent,
}
