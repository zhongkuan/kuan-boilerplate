import qs from 'kuan-utils/lib/qs'

import du from './du' // 百度统计
import so from './360' // 360统计
import ga from './ga' // google统计
import tou from './tou' // 今日头条
import gdt from './gdt' // 广点通统计

// 环境变量
const isDev = process.env.NODE_ENV === 'development'

// 统计账号
const keys = {
	du: '14dafd612308f625e666e6628ed0902d',
	so: '',
	ga: '',
	tou: '1654949618170894',
}

// 统计标识
const { hmsr } = qs.parse() || {}

// 初始化统计
function init(id = '') {
	du.init(keys.du)
	so.init(keys.du)
	ga.init(keys.du)
	if (hmsr === 'jrtt') {
		tou.init(id || keys.tou)
	}
	if (hmsr === 'gdt') {
		gdt.init('1110141635')
	}
	if (hmsr === 'WXXXL') {
		gdt.init('1110221564')
	}
}

// 统计PV
function trackPageView(href) {
	if (isDev) return
	du.trackPageView(href)
	ga.trackPageView(href)
}

// 事件统计
function trackEvent(category, action, opt_label, opt_value) {
	if (isDev) return
	du.trackEvent(category, action, opt_label, opt_value)
	ga.trackEvent(category, action, opt_label, opt_value)
}

// 广点通统计
function trackTdt(data, type) {
	if (['lichengMP', 'gdt', 'WXXXL'].includes(hmsr)) {
		gdt.track(data, type)
	}
}

// 统计表单提交
function trackSubmit(id = '1655425314450436') {
	if (hmsr === 'jrtt') {
		tou.push(id)
	}
	trackEvent('表单提交', window.location.href)
}

// 自动初始化统计
if (!isDev) init()

export default {
	init,
	trackPageView,
	trackEvent,
	trackTdt,
	trackSubmit,
}
