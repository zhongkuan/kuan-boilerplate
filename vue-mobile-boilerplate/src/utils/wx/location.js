import { getWechatConfig, loadWxSdk } from './tools.js'

export async function openLocation(option) {
  const [config] = await Promise.all([getWechatConfig(), loadWxSdk()])
  let fixOptions = option
  if (!option) {
    const { latitude, longitude } = await getLocation
    fixOptions = { latitude, longitude }
  }
  wx.config(config)
  wx.ready(() => {
    wx.openLocation({
      latitude: 0, // 纬度，浮点数，范围为90 ~ -90
      longitude: 0, // 经度，浮点数，范围为180 ~ -180。
      name: '', // 位置名
      scale: 6, // 地图缩放级别,整形值,范围从1~28。默认为最大
      ...fixOptions
    })
  })
}

export async function getLocation() {
  const [config] = await Promise.all([getWechatConfig(), loadWxSdk()])
  wx.config(config)
  return new Promise(resolve => {
    wx.ready(() => {
      wx.getLocation({
        type: 'wgs84', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
        success: function(res) {
          resolve(res)
          //   var latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
          //   var longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
          //   var speed = res.speed; // 速度，以米/每秒计
          //   var accuracy = res.accuracy; // 位置精度
        }
      })
    })
  })
}

export default {
  get: getLocation,
  open: openLocation
}
