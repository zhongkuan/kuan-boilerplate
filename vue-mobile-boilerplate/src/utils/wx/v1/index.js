import * as tools from './tools'
import pay from './pay'
import location from './location'
import share from './share'

export default {
  tools,
  pay,
  share,
  location
}
