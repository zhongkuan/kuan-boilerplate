import share from 'kuan-utils/lib/wxShare'
import { isWechat, getWechatConfig } from './tools'

const isDev = process.env.NODE_ENV === 'development'

// 默认分享数据
function getDefults() {
  return {
    title: document.title || '幻熊科技', // 分享标题
    desc: getDesc('这里没有描述'), // 分享描述
    link: window.location.href, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
    imgUrl: require('@/assets/logo.png') // 分享图标
  }
}

// 发起分享
export default async (shareData = getDefults()) => {
  if (!isWechat) return
  if (isDev) return console.table([shareData])
  const config = await getWechatConfig()
  share(shareData, config)
}

// 获取meta标签中的描述信息
function getDesc(desc = '') {
  const metas = [...document.querySelectorAll('meta')]
  const meta = metas.find(item => item.name === 'description')
  return meta ? meta.content : desc
}
