import share from 'kuan-utils/lib/wxShare'
import { isWx as isWechat } from 'kuan-utils/lib/validate'
import { getWechatConfig } from './tools'

const isDev = process.env.NODE_ENV === 'development'

// 发起分享
export default async (shareData = {}) => {
  if (isDev) return
  if (!isWechat) return
  const data = { ...getDefults(), ...shareData }
  // if (isDev) return console.table([data])
  const config = await getWechatConfig()
  share(data, config)
}

// 默认分享数据
function getDefults() {
  return {
    title: document.title || '幻熊科技', // 分享标题
    desc: getDesc('这里没有描述'), // 分享描述
    link: window.location.href, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
    imgUrl: window.location.origin + require('./images/banner.png') // 分享图标
  }
}

// 获取meta标签中的描述信息
function getDesc(desc = '') {
  const metas = [...document.querySelectorAll('meta')]
  const meta = metas.find(item => item.name === 'description')
  return meta ? meta.content : desc
}
