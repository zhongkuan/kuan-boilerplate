import 'kuan-css/index.css'

import Vue from 'vue'
import { toast } from '@halobear/vue-request'

import request from '@/utils/request'

import App from './App.vue'
import router from './router'
import store from './store'

import './styles/index.less'

Vue.prototype.$http = request
Vue.prototype.$bus = new Vue()
Vue.prototype.$toast = toast

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app')
