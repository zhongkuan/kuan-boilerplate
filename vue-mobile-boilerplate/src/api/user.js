import request from '@/utils/request'

export function login({ phone, code, act_id }) {
	return request({
		url: '/api/client/v1/bind/phone',
		method: 'post',
		data: {
			phone,
			code,
			act_id,
		},
	})
}

export function logout() {
	return Promise.resolve()
}

export function wxLogin(code) {
	return request({
		url: '/api/client/v1/auth/mp/login',
		method: 'POST',
		data: { token: code },
	})
}
