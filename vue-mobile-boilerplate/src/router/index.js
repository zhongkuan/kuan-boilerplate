import Vue from 'vue'
import VueRouter from 'vue-router'

import statistics from '@/utils/statistics'
import routes from './routes'

Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
	scrollBehavior(to, from, savedPosition) {
		return savedPosition || { x: 0, y: 0 }
	},
})

router.afterEach(route => {
	// 设置路由title
	if (route.meta && route.meta.title) {
		document.title = route.meta.title
	}

	// 拿到正确的路径，多以添加延迟
	setTimeout(() => {
		const href = process.env.BASE_URL.replace(/\/$/, '') + route.fullPath
		statistics.trackPageView(href) // 百度统计
	}, 300)
})

export default router
