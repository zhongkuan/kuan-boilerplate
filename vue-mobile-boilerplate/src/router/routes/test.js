export default [
	{
		name: 'debug',
		path: '/debug',
		component: () => import('../../views/debug/index'),
		meta: {
			title: '调试',
		},
	},
	{
		path: '/test',
		name: 'test',
		component: () => import('../../views/test/index'),
		meta: {
			title: '测试',
		},
	},
]
