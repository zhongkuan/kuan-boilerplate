const files = require.context('.', false, /\.js$/)
let routes = []

files.keys().forEach(key => {
	if (key === './index.js') return
	const route = files(key).default
	if (Array.isArray(route)) {
		routes = [...routes, ...route]
	} else {
		routes.push(route)
	}
})

routes.push({
	path: '*',
	redirect: { name: 'home' },
})

export default routes
