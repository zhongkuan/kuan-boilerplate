export default [
  {
    path: '/',
    name: 'home',
    component: () => import('../../views/home/index'),
    meta: {
      title: '幻熊科技',
    },
  },
]
