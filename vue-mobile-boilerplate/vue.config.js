const path = require('path')

const isDev = process.env.NODE_ENV !== 'production'
const proxy = 'https://activity.weddingee.com' // 测试地址
// const proxy = 'https://activity.shangehun.com' // 正式地址

module.exports = {
	publicPath: isDev ? '/' : `/h5/wedding/`, // public path
	outputDir: 'dist/wedding', // 输入地址
	assetsDir: 'static',
	devServer: {
		proxy,
	},
	pluginOptions: {
		'style-resources-loader': {
			preProcessor: 'less',
			patterns: [path.resolve(__dirname, './src/styles/variables.less')],
		},
	},
	productionSourceMap: false,
	chainWebpack: config => {
		// 移除 prefetch 插件
		config.plugins.delete('prefetch')
	},
}
