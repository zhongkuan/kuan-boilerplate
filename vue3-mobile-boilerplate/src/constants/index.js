const isDev = process.env.NODE_ENV !== 'production'

const PROXY_DEV = ''
// const PROXY_DEV = 'https://app-renren-dev.weddingee.com'
const PROXY_PROD = 'https://app.renrenyan.com'

// 接口默认域名
export const PROXY = isDev ? PROXY_DEV : PROXY_PROD

