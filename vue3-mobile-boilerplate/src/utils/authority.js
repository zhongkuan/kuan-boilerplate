import localData from '@halobear/utils/localData'

const TWENTY_HOURS = 60 * 20

const key = process.env.VUE_APP_AUTH || 'vue-boilerplate'

export default localData.create(key, TWENTY_HOURS)
