import Instance from '../BodyInstance.js'

import Confirm from '@/common/confirm-dialog'

let confirmDialog
export default (options = { title: '', desc: '', btn: '' }) => {
  if (!confirmDialog) {
    confirmDialog = new Instance(Confirm)
  }
  return new Promise((resolve) => {
    confirmDialog.show(options, resolve)
  })
}
