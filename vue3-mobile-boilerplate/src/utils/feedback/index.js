import { loading, toast } from '@halobear/request'

import confirmResult from './confirmResult'

export {
  loading,
  toast,
  confirmResult
}