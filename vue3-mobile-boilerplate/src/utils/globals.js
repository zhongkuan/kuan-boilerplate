import { toast, loading } from './feedback'

export default {
  install(app) {
    app.config.globalProperties.$toast = toast
    app.config.globalProperties.$loading = loading
  }
}