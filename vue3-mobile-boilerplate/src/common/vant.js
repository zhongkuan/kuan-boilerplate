import { NumberKeyboard } from 'vant'

export default {
  install(app) {
    app.use(NumberKeyboard)
  }
}
