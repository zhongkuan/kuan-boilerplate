import request from '@/utils/request'

export function panoInfo({ hall_id, hotel_id, bar_height, is_old }) {
  return request({
    url: `/api/app-renren/v1/pano/hall/${hall_id}`,
    params: { bar_height, hotel_id, is_old },
  })
}
