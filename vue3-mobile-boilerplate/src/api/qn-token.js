import request from '@/utils/request'

let cacheToken = ''

export async function qnToken() {
  if (cacheToken) return cacheToken
  cacheToken = await request({
    url: '/api/h5-bear/v1/qiniu/token',
    shouldLoading: false
  })
  return cacheToken
}