import request from '@/utils/request'

export function login(screen_key) {
  return request({
    url: '/api/screen/v1/pc/authorization',
    method: 'post',
    data: { screen_key },
  })
}

