import { createRouter, createWebHistory } from 'vue-router'

import routes from './routes'

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  scrollBehavior: () => ({ y: 0 }),
  routes,
})

router.afterEach((route) => {
  // 设置路由title
  const { title = process.env.VUE_APP_TITLE } = route.meta || {}
  title && (document.title = title)
})

export default router
