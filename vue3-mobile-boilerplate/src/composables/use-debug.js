import { onMounted } from 'vue'
import { useRoute } from 'vue-router'
import vconsole from '@halobear/utils/vconsole'

export default () => {
  const route = useRoute()
  onMounted(() => {
    setTimeout(() => {
      if (route.query.debug) {
        vconsole.show()
      }
    }, 1000)
  })

  return { vconsole }
}