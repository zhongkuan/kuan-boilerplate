import { createApp } from 'vue'
import rem from '@halobear/utils/rem'
import App from './App.vue'
import router from './router'
// import store from './store'
import vant from './common/vant'
import globals from './utils/globals'

import './styles/index.less'

// 动态设置rem
rem()

createApp(App)
  .use(globals)
  .use(router)
  // .use(store)
  .use(vant)
  .mount('#app')
