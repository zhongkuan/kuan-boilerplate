const path = require('path')
const fs = require('fs')
const compressing = require('compressing')

const resolve = (...dir) => path.resolve(__dirname, '..', 'dist', ...dir)

// 压缩dist
async function start() {
  const targetDir = resolve('crm_bear')
  const zipPath = resolve('src-crm_bear.zip')
  if (fs.existsSync(zipPath)) fs.unlinkSync(zipPath)
  await compressing.zip.compressDir(targetDir, zipPath)
  console.log('压缩完成!')
}

start()
