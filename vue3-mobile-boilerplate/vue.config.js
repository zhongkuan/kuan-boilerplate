const nodeEnv = process.env.NODE_ENV
const publicMap = {
  development: '/',
  production: '/panos/',
  test: '/h5/panos/',
}

const outputMap = {
  development: 'dist/panos',
  production: 'dist/panos',
  test: 'dist/panos',
}


module.exports = {
  publicPath: publicMap[nodeEnv], // public path
  outputDir: outputMap[nodeEnv], // 输入地址
  assetsDir: 'static',
  devServer: {
    proxy: {
      '/api': {
        target: 'https://app-renren-dev.weddingee.com',
        changeOrigin: true,
      },
    },
  },
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          require("postcss-pxtorem")({
            rootValue: 100,
            propList: ["*"],
            unitPrecision: 5,
            selectorBlackList: [/^body$/, /^input$/, /^textarea$/, /^\.iconfont/, /^.one-px-*/, /^\.halo-*/, /^\.van-*/],
            mediaQuery: true,
            minPixelValue: 0,
          })
        ]
      }
    }
  }
}