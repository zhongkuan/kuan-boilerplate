# vite-electron-boilerplate

- src，前端代码，electron 渲染进程
- app/main，electron 主进程
- app/preload，electron 相关 api，vite 中无法使用 require，electron 相关 api，放在 api/preload 文件中
- @halobear/css 包单独靠谱出来，因为 iconfont 文件无法使用自动选择协议，需要写完成域名
