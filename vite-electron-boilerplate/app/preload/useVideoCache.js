const fs = require('fs-extra')

const { videoFormat } = require('./pathFormat')

module.exports = async (src) => {
  const cacheUrl = videoFormat(src)
  const exists = await fs.pathExists(cacheUrl)
  return exists ? `file://${cacheUrl}` : src
}
