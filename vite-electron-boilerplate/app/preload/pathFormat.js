const { join } = require('path')
const { api, images, video, xml, pano_images } = require('./pathMap')

function apiFormat(src = '', userId = '0') {
  const value = src.replace(/^\/api\/app-boe\//, '').replace(/(\?|\=)/g, '_')
  return join(api, userId, `${value}.json`)
}

function imageFormat(src = '') {
  const pathUrl = ((src.split('.com/')[1] || '').split('?')[0] || 'tmp.png').replace(
    /(\.[^.]+)(-w?\d+x?\d*n?$)/,
    ($0, $1) => `${$0}${$1}`
  )
  return join(images, ...pathUrl.split('/'))
}

function videoFormat(src = '') {
  const pathUrl = (src.split('.com/')[1] || '').split('?')[0] || 'tmp.mp4'
  return join(video, ...pathUrl.split('/'))
}

function xmlFormat(title = '') {
  return join(xml, ...title.replace(/(\?|\=)/g, '_').split('/'))
}

function panoZipFormat(src = '') {
  return join(pano_images, src.split('/').pop())
}

module.exports = {
  apiFormat,
  imageFormat,
  videoFormat,
  xmlFormat,
  panoZipFormat,
}
