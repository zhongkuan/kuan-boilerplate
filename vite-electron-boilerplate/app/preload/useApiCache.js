const fs = require('fs-extra')

const { apiFormat } = require('./pathFormat')

module.exports = async (url, userId) => {
  const cacheUrl = apiFormat(url, userId)
  const exists = await fs.pathExists(cacheUrl)
  return exists ? fs.readJSON(cacheUrl) : ''
}
