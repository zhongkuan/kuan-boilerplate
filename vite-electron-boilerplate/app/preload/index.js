const { contextBridge, ipcRenderer } = require('electron')

const pathFormat = require('./pathFormat')
const { pano: panoHTML } = require('./pathMap')
const { removeOfflineFile } = require('./download/utils')
const useImageCache = require('./useImageCache')
const useVideoCache = require('./useVideoCache')
const useApiCache = require('./useApiCache')

// download: (data) => ipcRenderer.send('download', data),
// download2: (data) => ipcRenderer.invoke('download2', data),

contextBridge.exposeInMainWorld('electron', {
  ipcRenderer,
  removeOfflineFile,
  useImageCache,
  useVideoCache,
  useApiCache,
  pathFormat,
  panoHTML,
})
