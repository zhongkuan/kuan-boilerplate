const path = require('path')
const os = require('os')

const isDev = process.env.NODE_ENV === 'development'

// 额外资源目录
const extraResources = isDev
  ? path.join(__dirname, '../extraResources')
  : path.join(process.resourcesPath, 'app/extraResources')

// 用户家目录
const homedir = isDev ? path.join(extraResources, 'home') : os.homedir()
// 额外目录工具类
const resolve = (...dir) => path.join(extraResources, ...dir)
const resolveCache = (...dir) => path.join(homedir, '.lichengviteos', 'cache', ...dir)

// 接口存放目录
const api = resolveCache('api')

// 图片存放目录
const images = resolveCache('images')

// 视频存放目录
const video = resolveCache('video')

// 全景html
const pano = resolve('pano', 'index.html')

// 全景xml存放目录
const xml = resolve('pano', 'xml')

// 全景图片存放目录
const pano_images = resolve('store')

module.exports = {
  extraResources,
  homedir,
  api,
  images,
  video,
  pano,
  xml,
  pano_images,
}
