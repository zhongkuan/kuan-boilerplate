const fs = require('fs-extra')

const { imageFormat } = require('./pathFormat')

module.exports = async (src) => {
  const cacheUrl = imageFormat(src)
  const exists = await fs.pathExists(cacheUrl)
  return exists ? `file://${cacheUrl}` : src
}
