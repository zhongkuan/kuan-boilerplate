const devToolVisible = process.env.NODE_ENV === 'development'
// const devToolVisible = true

module.exports = {
  devToolVisible,
}
