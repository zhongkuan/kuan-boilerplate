const { ipcMain } = require('electron')

// 通信处理渲染进程
module.exports = () => {
  ipcMain.on('download', async (data) => {
    await new Promise((resolve) => setTimeout(resolve, 1000))
  })

  ipcMain.handle('download2', async (e, data) => {
    return data.toString()
  })
}
