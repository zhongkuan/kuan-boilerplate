let now = 0
let status = true

export default () => {
  return new Promise((resolve, reject) => {
    const newNow = new Date().getTime()
    if (now + 500 < newNow) {
      now = newNow
      resolve()
    } else {
      reject("点击频率过快")
    }
  })
}