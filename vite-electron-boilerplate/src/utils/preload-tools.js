const {
  ipcRenderer,
  removeOfflineFile,
  panoHTML,
  useImageCache,
  useVideoCache,
  useApiCache,
  pathFormat = {},
  downloadApi,
  downloadImages,
  downloadVideo,
  downloadPanos,
} = window.electron || {}

const { apiFormmat, imageFormat, videoFormat, xmlFormat, panoZipFormat } = pathFormat

export {
  ipcRenderer,
  removeOfflineFile,
  useImageCache,
  useVideoCache,
  useApiCache,
  panoHTML,
  apiFormmat,
  imageFormat,
  videoFormat,
  xmlFormat,
  panoZipFormat,
  downloadApi,
  downloadImages,
  downloadVideo,
  downloadPanos,
}
