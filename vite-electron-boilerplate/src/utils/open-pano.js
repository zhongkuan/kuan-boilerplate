import { createApp } from 'vue'

import PanoView from '@/components/pano-view/index.vue'

let app

function openPano(hotel_id) {
  if (!app) {
    const div = document.createElement('div')
    document.body.appendChild(div)
    const instance = createApp(PanoView)
    app = instance.mount(div)
  }
  app.show(hotel_id)
}

export default openPano
