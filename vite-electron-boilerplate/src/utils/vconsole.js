import vconsole from '@halobear/utils/vconsole'
import qs from '@halobear/utils/qs'

const isDev = qs.get('console', window.location.search.split('?')[1] || '')

if (isDev) {
  vconsole.init()
}

export default vconsole
