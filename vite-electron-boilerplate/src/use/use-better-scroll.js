import { onBeforeUnmount, onMounted, unref, ref } from 'vue'

import BScroll from '@better-scroll/core'

export default (el) => {
  const bs = ref()
  onMounted(() => {
    bs.value = new BScroll(unref(el), {
      scrollX: true,
    })
  })

  onBeforeUnmount(() => {
    bs.value && bs.value.destroy()
  })
  return { bs }
}