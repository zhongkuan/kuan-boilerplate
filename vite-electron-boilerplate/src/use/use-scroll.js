import { onBeforeUnmount, onMounted, ref } from 'vue'

export default () => {
  const top = ref(0)

  const handleScroll = () => {
    top.value = document.documentElement.scrollTop
  }

  onMounted(() => {
    window.addEventListener('scroll', handleScroll)
  })

  onBeforeUnmount(() => {
    window.removeEventListener('scroll', handleScroll)
  })

  return {
    top,
  }
}
