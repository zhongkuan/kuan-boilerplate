import { RouterView } from 'vue-router'

export default [
  {
    name: 'home',
    path: '/home',
    component: () => import('@/views/home/index.vue'),
    meta: { title: '首页' },
  },
]
