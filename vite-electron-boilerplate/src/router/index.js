import { createRouter, createWebHashHistory, RouterView } from 'vue-router' // there is also  createWebHistory createWebHashHistory and createMemoryHistory

import store from '/src/store/index'

import Login from '../views/login/index.vue'

import routes from './routes'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      name: 'index',
      component: RouterView,
      meta: { title: '管理后台' },
      redirect: { path: '/home' },
      children: routes,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: { title: '登录页面' },
    },
    { path: '/:noMatch(.*)', redirect: { path: '/' } },
  ],
})

router.beforeEach((to, from) => {
  const { token } = store.state.user.user
  if (!token && to.name !== 'login') return '/login'
})

export default router
