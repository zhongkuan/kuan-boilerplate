import * as UserApi from '/src/api/user'
import router from '/src/router'

const state = {
  user: {},
}

const mutations = {
  SET_USER(state, payload) {
    Object.assign(state, payload)
  },
}

const actions = {
  async login({ commit }, { phone = '', code = '' }) {
    const { user: data = {}, token } = await UserApi.login({ phone, code })
    data.token = token
    commit('SET_USER', { user: data })
  },
  logout({ commit }) {
    commit('SET_USER', { token: '', name: '' })
    router.push({ name: 'login' })
  },
}

export default { state, mutations, actions }
