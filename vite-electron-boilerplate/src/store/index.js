import { createStore } from 'vuex'

import persistedState from './plugins/persisted-state'
import resetState from './plugins/reset-state'

import user from './modules/user'

export default createStore({
  modules: {
    user,
  },
  plugins: [persistedState, resetState],
})
