import './assets/styles/index.less'

import { createApp } from 'vue'

import App from './App.vue'

// (警告忽略，使用vite这样引入包更小) You are using a whole package of antd,
import common from './common'
import store from './store'
import router from './router'

import '/src/utils/vconsole'

const app = createApp(App)

app.use(store)
app.use(router)
app.use(common)

app.mount('#app')
