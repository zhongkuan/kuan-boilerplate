import request from '@/utils/request'

export function list() {
  return request({
    url: `/api/app-boe/v1/travel/merchant/destination/hotel`,
  })
}

export function detail(id) {
  return request({
    url: `/api/app-boe/v1/travel/merchant/destination/hotel/${id}`,
  })
}
