import request from '/src/utils/request'

export function login({ phone, code }) {
  return request({
    url: '/api/app-boe/v1/travel/merchant/login',
    method: 'post',
    data: {
      phone: phone.trim(),
      code: code.trim(),
    },
    shouldLoading: true,
  })
}

export function logout() {
  return Promise.resolve()
}

// 获取验证码
export function getCode(data) {
  return request({
    url: '/api/app-travel-file/v1/phone/code',
    method: 'post',
    data,
    shouldLoading: true,
  })
}
