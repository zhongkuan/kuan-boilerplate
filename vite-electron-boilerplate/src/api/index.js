import request from "/src/utils/request"

export function fetchCode(phone) {
  return request({
    url: "/api/app-boe/v1/phone/code",
    method: "post",
    data: { phone }
  })
}


export function getHomePage() {
  return request({
    url: `/api/app-boe/v1/travel/merchant/index`,
  })
}