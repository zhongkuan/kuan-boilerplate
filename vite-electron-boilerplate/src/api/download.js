import request from '@/utils/request'

const offlineCache = {
  time: 0,
  data: {},
}

export async function offlineData(shouldCache = true) {
  const now = Date.now()
  if (shouldCache && now - offlineCache.time < 1000 * 60 * 60) return offlineCache.data
  offlineCache.data = await request({
    url: '/api/app-boe/v1/travel/merchant/data',
    shouldLoading: true,
  })
  offlineCache.time = now
  return offlineCache.data
}
