import request from '../utils/request'

let token
let preFetch

export async function toGetToken() {
  const res = await request({
    url: '/api/app-travel-file/v1/qiniu/token',
  })
  token = res.qiniu_token
  preFetch = null
  return token
}

// 防止重复发送请求
export function getToken() {
  if (token) return token
  if (preFetch) return preFetch
  preFetch = toGetToken()
  return preFetch
}
