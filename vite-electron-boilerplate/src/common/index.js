import antd from './antd'
import icon from './icon'
import { Swiper, SwiperSlide } from './swiper'

import MyDialog from './my-dialog/index.vue'
import MyImage from './my-image/index.vue'
import BlankPage from './blank-page/index.vue'
import CloseIcon from './close-icon/index.vue'

const components = [Swiper, SwiperSlide, MyDialog, MyImage, BlankPage, CloseIcon]

export default (app) => {
  app.use(antd)
  app.use(icon)

  components.forEach((component) => {
    app.component(component.name, component)
  })
}
