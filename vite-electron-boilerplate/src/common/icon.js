import { createFromIconfontCN, HomeOutlined } from '@ant-design/icons-vue'

const IconFont = createFromIconfontCN({
  // scriptUrl: 'https://at.alicdn.com/t/font_2504426_2hohdhxeg2b.js',
})
IconFont.displayName = 'icon-font'

const components = [IconFont, HomeOutlined]

export default (app) => {
  components.forEach((component) => {
    app.component(component.displayName, component)
  })
}
