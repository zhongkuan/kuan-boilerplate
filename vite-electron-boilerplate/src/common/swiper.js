import { mapState } from 'vuex'
import { Swiper, SwiperSlide } from 'swiper/swiper-vue.esm'
import SwiperCore, { Autoplay, Navigation } from 'swiper'

SwiperCore.use([Autoplay, Navigation])

import 'swiper/swiper.less'
import 'swiper/components/effect-coverflow/effect-coverflow.less'

export { Swiper, SwiperSlide }
