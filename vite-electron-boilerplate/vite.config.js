import path from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'

// https://vitejs.dev/config/
export default defineConfig({
  base: './',
  server: {
    port: 5999,
    proxy: {
      '/api': 'todo',
    },
    cors: true,
  },
  resolve: {
    alias: {
      '@': path.join(__dirname, 'src'),
    },
  },
  plugins: [vue(), vueJsx()],
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },
  build: {
    assetsDir: 'static',
    outDir: 'app/renderer',
    chunkSizeWarningLimit: 1024 * 2, // kb
  },
})
