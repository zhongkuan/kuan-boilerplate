# kuan-boilerplate

当前分支为 `kuan-cli` 生成项目的模板仓库（原模板项目 已迁移`boilerplate` 分支）

## 使用

```bash
# 安装
npm install -g kuan-cli
# 创建项目
kuan-cli init -d <rollup-ts> <app-name>
```
