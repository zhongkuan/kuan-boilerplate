import statistics from '@/utils/statistics'

/*
 ** 只在生成模式的客户端中使用
 */
if (process.browser) {
  statistics.init()
}

export default ({ app: { router } }) => {
  router.afterEach((to) => {
    if (process.browser) {
      statistics.trackPageView(to.fullPath)
    }
  })
}
