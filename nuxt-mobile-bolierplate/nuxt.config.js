import { PROXY } from './constants'

const isDev = process.env.NODE_ENV === "development"

export default {
  head: {
    title: "<%= title %>",
    meta: [
      { charset: 'utf-8' },
      { name: "viewport", content: `width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0` },
      { name: 'keywords', content: '<%= description %>' },
      { name: 'description', content: '<%= description %>' },
      { name: "format-detection", content: "telephone=no,email=no" },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  proxy: {
    "/api": PROXY
  },

  loading: { color: "#0185F2" },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ["@halobear/css", "./styles/index.less"],
  styleResources: {
    less: ["./styles/variables.less"]
  },

  /*
 ** Plugins to load before mounting the App
 */
  plugins: [
    { src: "~plugins/$bus.js" },
    { src: "~plugins/$http.js" },
    { src: "~plugins/statistics.js", mode: "client" }, // 统计插件
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: ["@nuxtjs/style-resources", "@nuxtjs/proxy"],

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    ...(isDev ? {} : { publicPath: '/client/' }),
    extractCSS: true,
    loaders: {
      fontUrl: { limit: 1 },
      imgUrl: { limit: 1 }
    }
  },
  // router: {
  //   extendRoutes(routes) {
  //     routes.push({
  //       path: "*",
  //       components:'error'
  //     });
  //   }
  // }
}
