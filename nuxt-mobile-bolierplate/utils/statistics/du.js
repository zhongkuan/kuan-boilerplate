/* eslint-disable */
let isInit = false

// 初始化统计
function init(id) {
  if (!id) return
  var hm = document.createElement('script')
  hm.src = `https://hm.baidu.com/hm.js?${id}`
  var s = document.getElementsByTagName('script')[0]
  s.parentNode.insertBefore(hm, s)
  isInit = true
}

// 统计页面访问
function trackPageView(href) {
  if (!isInit) return
  if (typeof _hmt === 'undefined') return
  _hmt.push(['_trackPageview', href])
}

// 事件统计
function trackEvent(category, action, opt_label, opt_value) {
  if (!isInit) return
  if (typeof _hmt === 'undefined') return
  _hmt.push(['_trackEvent', category, action, opt_label, opt_value])
}

export default {
  init,
  trackPageView,
  trackEvent
}
