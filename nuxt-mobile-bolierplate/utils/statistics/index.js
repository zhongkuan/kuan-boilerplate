import du from './du' // 百度统计

const isDev = process.env.NODE_ENV === 'development'

// 统计账号
const keys = {
  du: 'acfeb73e69020cb8c3eaeaa369d996b0',
}

export default {
  init() {
    if (isDev) return
    du.init(keys.du)
  },
  trackPageView(href) {
    if (isDev) return
    du.trackPageView(href)
  },
  trackEvent(category, action, opt_label, opt_value) {
    if (isDev) return
    du.trackEvent(category, action, opt_label, opt_value)
  }
}
