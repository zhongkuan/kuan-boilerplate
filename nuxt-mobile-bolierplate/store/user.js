import authority from "@/utils/authority"

export const state = () => ({
  token: '',
  user: authority.get() || {},
})

export const mutations = {
  SET_USER(state, payload) {
    Object.assign(state, payload)
  }
}

export const actions = {
  async login({ commit }, payload) {
    console.log(payload)
    commit("SET_USER", user)
  },
  async logout({ commit }) {
    authority.clear()
    commit("SET_USER_INFO", { user: {}, token: "" })
  },
}
