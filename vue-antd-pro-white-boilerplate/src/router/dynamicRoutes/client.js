import { RouteView } from "@/layouts/index";

export default [
  {
    path: "/client/netSale",
    component: RouteView,
    name: "netSaleParent",
    meta: { title: "网销中心", icon: "desktop" },
    hideChildrenInMenu: true,
    redirect: { name: "netSale" },
    children: [
      {
        path: "/client/netSale",
        name: "netSale",
        meta: { title: "网销中心", hideBreadcrumb: true },
        component: () => import("@/views/client/netSale/index.vue")
      },
      {
        path: "/client/netSale/detail",
        name: "netSaleDetail",
        meta: { title: "网销详情" },
        component: () => import("@/views/client/detail/index.vue")
      }
    ]
  },
  {
    path: "/client/telscale",
    component: RouteView,
    name: "telScaleParent",
    meta: { title: "电销中心", icon: "phone" },
    hideChildrenInMenu: true,
    redirect: { name: "telScale" },
    children: [
      {
        path: "/client/telscale",
        name: "telScale",
        meta: { title: "电销中心", hideBreadcrumb: true },
        component: () => import("@/views/client/telSale/index.vue")
      },
      {
        path: "/client/telscale/detail",
        name: "telScaleDetail",
        meta: { title: "电销详情" },
        component: () => import("@/views/client/detail/index.vue")
      }
    ]
  },
  {
    path: "/client/faceSale",
    component: RouteView,
    name: "faceSaleParent",
    meta: { title: "面销中心", icon: "meh" },
    hideChildrenInMenu: true,
    redirect: { name: "faceSale" },
    children: [
      {
        path: "/client/faceSale",
        name: "faceSale",
        meta: { title: "面销中心", hideBreadcrumb: true },
        component: () => import("@/views/client/faceSale/index.vue")
      },
      {
        path: "/client/faceSale/detail",
        name: "faceSaleDetail",
        meta: { title: "面销详情" },
        component: () => import("@/views/client/detail/index.vue")
      }
    ]
  }
];
