import request from "@/utils/request";

export function list(params) {
  return request({
    url: "/api/halo-dashboard/v1/product/brand",
    params
  });
}

export function add(data) {
  return request({
    url: "/api/halo-dashboard/v1/product/brand",
    method: "post",
    data
  });
}

export function detail(id) {
  return request({
    url: `/api/halo-dashboard/v1/product/brand/${id}`
  });
}

export function update(data) {
  return request({
    url: `/api/halo-dashboard/v1/product/brand/${data.id}`,
    method: "put",
    data
  });
}

export function remove(id) {
  return request({
    url: `/api/halo-dashboard/v1/product/brand/${id}`,
    method: "delete"
  });
}

export function change(data) {
  const url = !data.is_enable
    ? `/api/halo-dashboard/v1/product/brand/${data.id}/resume`
    : `/api/halo-dashboard/v1/product/brand/${data.id}/forbid`;
  return request({
    url,
    method: "patch"
  });
}
