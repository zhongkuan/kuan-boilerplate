import request from "@/utils/request";

export function list(params) {
  return request({
    url: "/api/halo-dashboard/v1/product/system/attribute/option",
    params
  });
}

export function add(data) {
  return request({
    url: "/api/halo-dashboard/v1/product/system/attribute/option",
    method: "post",
    data
  });
}

export function detail(id) {
  return request({
    url: `/api/halo-dashboard/v1/product/system/attribute/option/${id}`
  });
}

export function update(data) {
  return request({
    url: `/api/halo-dashboard/v1/product/system/attribute/option/${data.id}`,
    method: "put",
    data
  });
}

export function remove(id) {
  return request({
    url: `/api/halo-dashboard/v1/product/system/attribute/option/${id}`,
    method: "delete"
  });
}
