import request from "@/utils/request";

export function list(params) {
  return request({
    url: "/api/halo-dashboard/v1/product/supplier",
    method: "get",
    params
  });
}

export function add(params) {
  return request({
    url: "/api/halo-dashboard/v1/product/supplier",
    method: "post",
    params
  });
}

export function update(data) {
  return request({
    url: `/api/halo-dashboard/v1/product/supplier/${data.id}`,
    method: "put",
    data
  });
}

// 禁用
export function close(id) {
  return request({
    url: `/api/halo-dashboard/v1/product/supplier/${id}/forbid`,
    method: "patch"
  });
}
//启用
export function open(id) {
  return request({
    url: `/api/halo-dashboard/v1/product/supplier/${id}/resume`,
    method: "patch"
  });
}

// 删除
export function remove(id) {
  return request({
    url: `/api/halo-dashboard/v1/product/supplier/${id}`,
    method: "delete"
  });
}
