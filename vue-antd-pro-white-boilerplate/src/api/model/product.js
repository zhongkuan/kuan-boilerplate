import request from "@/utils/request";

export function list(params) {
  return request({
    url: "/api/halo-dashboard/v1/product",
    params
  });
}

export function add(data) {
  return request({
    url: "/api/halo-dashboard/v1/product",
    method: "post",
    data
  });
}

export function detail(id) {
  return request({
    url: `/api/halo-dashboard/v1/product/${id}`
  });
}

export function update(data) {
  return request({
    url: `/api/halo-dashboard/v1/product/${data.id}`,
    method: "put",
    data
  });
}

export function remove(id) {
  return request({
    url: `/api/halo-dashboard/v1/product/${id}`,
    method: "delete"
  });
}

export function change(data) {
  const url = !data.is_enable
    ? `/api/halo-dashboard/v1/product/${data.id}/resume`
    : `/api/halo-dashboard/v1/product/${data.id}/forbid`;
  return request({
    url,
    method: "patch"
  });
}
