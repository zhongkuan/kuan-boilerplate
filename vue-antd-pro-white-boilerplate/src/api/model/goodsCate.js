import request from "@/utils/request";

export function getGoodsCate(params) {
  return request({
    url: "/api/halo-dashboard/v1/product/category",
    method: "get",
    params
  });
}

export function add(data) {
  return request({
    url: "/api/halo-dashboard/v1/product/category",
    method: "post",
    data
  });
}

export function detail(id) {
  return request({
    url: `/api/halo-dashboard/v1/product/category/${id}`
  });
}

export function update(data) {
  return request({
    url: `/api/halo-dashboard/v1/product/category/${data.id}`,
    method: "put",
    data
  });
}

export function remove(id) {
  return request({
    url: `/api/halo-dashboard/v1/product/category/${id}`,
    method: "delete"
  });
}

export function change(data) {
  const url = !data.is_enable
    ? `/api/halo-dashboard/v1/product/category/${data.id}/resume`
    : `/api/halo-dashboard/v1/product/category/${data.id}/forbid`;
  return request({
    url,
    method: "patch"
  });
}
