import request from "@/utils/request";

export function getSpecList(params) {
  return request({
    url: "/api/halo-dashboard/v1/product/system/attribute",
    params
  });
}

export function add(data) {
  return request({
    url: "/api/halo-dashboard/v1/product/system/attribute",
    method: "post",
    data
  });
}

export function detail(id) {
  return request({
    url: `/api/halo-dashboard/v1/product/system/attribute/${id}`
  });
}

export function update(data) {
  return request({
    url: `/api/halo-dashboard/v1/product/system/attribute/${data.id}`,
    method: "put",
    data
  });
}

export function remove(id) {
  return request({
    url: `/api/halo-dashboard/v1/product/system/attribute/${id}`,
    method: "delete"
  });
}
