const state = {
  title: process.env.VUE_APP_HOME_TITLE,
  theme: 'light', // dark light
  copyright: '2021 幻熊科技',
}

const mutations = {
  SET_SETTING(state, payload) {
    Object.assign(state, payload)
  },
}

const actions = {
  setSetting({ commit }, setting) {
    commit('SET_SETTING', setting)
  },
}

export default {
  state,
  mutations,
  actions,
}
