import PageLayout from '@/layouts/PageLayout'
import antd from './antd'

// 在main.js中使用Vue.use(...), 会将同级目录下（包括子目录）的所有组件注册成全局组件
const components = {
  'page-layout': PageLayout
}
function requireAll(r) {
  return r.keys().map(key => {
    const name = key
      .replace(/^\.\//, '')
      .replace(/\/index.vue$/, '')
      .replace(/([A-Z])/g, '-$1')
      .toLowerCase()
    components[name] = r(key).default
  })
}
requireAll(require.context('./', true, /index\.vue/))

const install = Vue => {
  Vue.use(antd)
  Object.entries(components).forEach(([name, component]) => {
    Vue.component(name, component)
  })
}

export default {
  install
}

// // 全局组件注册
// import model from './myModel'
// import MyUpload from './myUpload' // 上传组件
// import RegionSelect from './regionSelect' // 地区选择
// import MyPagination from './myPagination' // 分页
// import MyImage from './myImage' // 预览图片
// import MyForm from './myForm' // 我的表单
// import MySearch from './mySearch'
//
// const install = (Vue) => {
//   Vue.component('my-model', model)
//   Vue.component('my-upload', MyUpload)
//   Vue.component('my-image', MyImage)
//   Vue.component('region-select', RegionSelect)
//   Vue.component('my-pagination', MyPagination)
//   Vue.component('my-form', MyForm)
//   Vue.component('my-search', MySearch)
// }
// export default {
//   install
// }
//
