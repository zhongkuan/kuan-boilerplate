import { message } from 'ant-design-vue'

export function imageInfo(file) {
  return new Promise((resolve, reject) => {
    if (!/^image\/[jpeg|png|jpg|gif|svg|ico]/gi.test(file.type)) {
      message.warning('请上传图片类型文件')
      return
    }
    const img = new Image()
    img.src = URL.createObjectURL(file)
    img.onload = function() {
      const { width, height } = this
      resolve({
        height,
        width
      })
    }
    img.onerror = e => {
      message.error('图片加载失败')
      reject(e)
    }
  })
}
