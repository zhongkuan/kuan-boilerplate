import { getFileMD5, getFiles, getFile, upload } from 'kuan-utils/lib/uploader'

export {
  getFileMD5,
  getFiles,
  getFile,
  upload
}

export default {
  getFileMD5,
  getFiles,
  getFile,
  upload
}