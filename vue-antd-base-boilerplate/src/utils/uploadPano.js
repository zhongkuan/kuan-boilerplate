import { loading } from '@halobear/request'

import * as Api from '@/api/place.js'
import { getFile } from '@/utils/uploader'

export default async (cut_type = 'normal') => {
  const { file } = await getFile({
    needMD5: false,
    accept: 'application/zip',
  })
  if (!file) return ''
  if (/\.zip$/.test(file.name)) {
    loading.show()
    try {
      // 上传全景散文件
      const { prefix } = await Api.uploadPano(file, cut_type === 'normal' ? 8 : 374)
      // 上传zip包
      await Api.uploadZip(prefix)
      return prefix
    } catch (e) {
      console.error(e)
    }
    loading.hide()
  }
  return ''
}
