import Vue from 'vue'
import { loading } from '@halobear/request'
import photoSwipe from 'kuan-vue-photoswipe'

import router from '@/router'
import store from '@/store'
import App from '@/App.vue'

import components from '@/common'
import mixins from '@/utils/mixins'
import request from '@/utils/request'

import './styles/index.less'

Vue.prototype.$http = request
Vue.prototype.$loading = loading
Vue.prototype.$bus = new Vue() // event bus

Vue.mixin(mixins) // mixins

Vue.use(photoSwipe) // 多图预览
Vue.use(components)

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount('#app')
