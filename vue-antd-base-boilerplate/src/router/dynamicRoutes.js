import BasicLayout from '@/layouts/BasicLayout'

const files = require.context('./routes', false, /\.js$/)
const routes = [
  {
    path: '/home',
    name: 'home',
    meta: { title: '幻熊科技', icon: 'home' },
    hidden: true,
    component: () => import('@/views/home'),
  },
]

files.keys().forEach((key) => {
  if (key === './index.js') return
  let route = files(key).default
  if (!Array.isArray(route)) {
    route = [route]
  }
  routes.push(...route)
})

export const menus = routes

export default [
  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: '幻熊科技' },
    redirect: 'home',
    children: routes,
  },
  {
    path: '*',
    redirect: { name: 'home' },
  },
]
