import RouteView from '@/layouts/RouteView'

export default [
  {
    path: '/blog',
    component: RouteView,
    name: 'blog',
    meta: { title: '博客管理', icon: 'book' },
    children: [
      {
        path: '/blog/list',
        name: 'blogList',
        component: () => import('@/views/blog/list'),
        meta: { title: '博客列表' },
      },
    ],
  },
]
