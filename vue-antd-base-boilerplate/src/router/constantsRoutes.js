import Login from '@/views/login'

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/test',
    name: 'test',
    component: () => import('@/views/test'),
  },
]

export default routes
