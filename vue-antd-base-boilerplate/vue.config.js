const isProd = process.env.NODE_ENV === 'production'

module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: process.env.VUE_APP_PROXY,
        changeOrigin: true,
      },
    },
  },
  productionSourceMap: false,
  publicPath: isProd ? '/h5/dashboard/' : '/',
  outputDir: 'dist/dashboard',
  assetsDir: 'static',
  chainWebpack: (config) => {
    // 删除 webpack 插件
    config.plugins.delete('preload')
    config.plugins.delete('prefetch')
  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true,
          globalVars: {
            'primary-color': '#1890ff',
          },
        },
      }
    }
  }
}