import { createApp } from 'vue'

import App from './App.vue'

// (警告忽略，使用vite这样引入包更小) You are using a whole package of antd,
import Antd from './common/antd'
import ProLayout from './common/pro-layout'
import icon from './common/icon'
import router from './router'

const app = createApp(App)
app.use(router)
app.use(Antd)
app.use(ProLayout)
app.use(icon)
app.mount('#app')
