import {ref} from 'vue'

export default () => {
  const n = ref(1)
  return (
    <>
      <a-avatar
        style="width: 30px; height: 30px"
        src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
      />
      <a-button onClick={() => n.value ++}>{n.value}</a-button>
    </>
  )
}
