import 'ant-design-vue/dist/antd.less'
import ProLayout, { PageContainer } from '@ant-design-vue/pro-layout'

export default (app) => {
  app.use(ProLayout).use(PageContainer)
}
