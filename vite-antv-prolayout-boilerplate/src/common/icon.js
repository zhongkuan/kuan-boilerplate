import { HomeOutlined, CreditCardOutlined, UserOutlined } from '@ant-design/icons-vue'

const components = [HomeOutlined, CreditCardOutlined, UserOutlined]

export default (app) => {
  components.forEach((component) => {
    app.component(component.displayName, component)
  })
}
