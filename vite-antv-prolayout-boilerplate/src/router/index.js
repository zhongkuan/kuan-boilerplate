import { createRouter, createWebHashHistory } from 'vue-router' // there is also  createWebHistory createWebHashHistory and createMemoryHistory
import Layout from '../components/layout/index.vue'

import routes from './routes'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      name: 'index',
      component: Layout,
      meta: { title: '管理后台' },
      children: routes,
    },
  ],
})

export default router
