import { RouterView } from 'vue-router'

export default [
  {
    name: 'home',
    path: '/home',
    component: () => import('/src/views/home/index.vue'),
    meta: { title: '管理后台', icon: 'HomeOutlined' },
  },
  {
    name: 'hotel',
    path: '/hotel',
    component: RouterView,
    meta: { title: '酒店管理', icon: 'CreditCardOutlined' },
    children: [
      {
        name: 'hotelList',
        path: '/hotel/list',
        component: () => import('/src/views/home/index.vue'),
        meta: { title: '酒店列表' },
      },
      {
        name: 'hotelSetting',
        path: '/hotel/setting',
        component: () => import('/src/views/home/index.vue'),
        meta: { title: '酒店分类' },
      },
    ],
  },
  {
    name: 'hotel2',
    path: '/hotel2',
    component: RouterView,
    meta: { title: '酒店管理2', icon: 'CreditCardOutlined' },
    children: [
      {
        name: 'hotelList2',
        path: '/hotel/list2',
        component: () => import('/src/views/home/index.vue'),
        meta: { title: '酒店列表' },
      },
      {
        name: 'hotelSetting2',
        path: '/hotel/setting2',
        component: () => import('/src/views/home/index.vue'),
        meta: { title: '酒店分类' },
      },
    ],
  },
  {
    name: 'test',
    path: '/test',
    component: () => import('/src/views/test/index.vue'),
    meta: { title: '测试', icon: 'CreditCardOutlined', hideInMenu: true },
  },
]
